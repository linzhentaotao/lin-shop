'use strict';
const path = require('path');
/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = (appInfo) => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = (exports = {});

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1648807776713_5902';
  config.static = {
    prefix: '/',
    dir: [
      path.join(__dirname, '../app/public/dist'),
      path.join(__dirname, '../dist'),
    ],
  };
  // add your middleware config here
  config.middleware = ['init', 'errorHanlder', 'token'];
  config.token = {
    ignore: [
      '/user/reg',
      '/user/login',
      '/user/wxLogin',
      '/api/seckill/upCookie',
      '/api/seckill/query',
    ], // tonke校验忽略注册和登陆的接口
  };
  // session 配置
  config.session = {
    key: 'EGG_SESS', // eggjs默认session的key
    maxAge: 24 * 3600 * 1000, // 1 day
    httpOnly: true,
    encrypt: true,
    renew: true, // 每次访问页面都会给session会话延长时间
  };
  config.security = {
    csrf: {
      enable: false,
    },
  };
  config.cors = {
    origin: '*',
    allowMethods: 'GET,HEAD,PUT,OPTIONSF,POST,DELETE,PATCH',
  };
  config.jwt = {
    secret: 'linzhentaoyyds', // 自定义 token 的加密条件字符串
  };
  config.axiosPlus = {
    headers: {
      common: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
      timeout: 5000, // 默认请求超时
      app: true, // 在app.js上启动加载
      agent: false, // 在agent.js上启动加载
    },
  };
  // config.multipart = {
  //   mode: 'file',
  //   fileSize: 1048576000,
  //   whitelist: [ '.wmv', '.png', '.mp4', '.mp3', '.jpg' ],
  // };
  config.multipart = {
    mode: 'stream',
    whitelist() {
      return true;
    },
    fileExtensions: ['.jpeg', '.wmv', '.png', '.mp4', '.mp3', '.jpg', '.zip'],
  };
  config.oss = {
    client: {
      accessKeyId: 'LTAI5tGyvfLhjiRnVJv2vRpt',
      accessKeySecret: 'wVOHnDXmJskCAxru5RR34l8nwABO62',
      bucket: 'lzt-cartoon',
      endpoint: 'oss-cn-shenzhen.aliyuncs.com',
      timeout: '60s',
    },
  };
  // config.redis = {
  //   client: {
  //     port: 6379,
  //     host: '127.0.0.1',
  //     password: '123456',
  //     db: 0,
  //   },
  // };
  // qq邮箱配置
  config.qqEmail = {
    host: 'smtp.qq.com', // QQ邮箱的SMTP地址
    port: 465, // 邮箱的端口号一般都使用465，
    auth: {
      user: '2030344925@qq.com', // 你自己的邮箱的邮箱地址
      pass: 'oiwttjwhoqtffhhd', // 授权码
    },
  };
  // 配置文件上传的模式
  // config.multipart = {
  //   mode: 'file',
  // };
  // 数据库
  config.mysql = {
    client: {
      host: 'localhost',
      port: '3306',
      user: 'root',
      password: 'root',
      database: 'shop',
    },
    // clients: {
    //   // 默认数据库
    //   cartoon: {
    //     // host
    //     host: 'localhost',
    //     // 端口号
    //     port: '3306',
    //     // 用户名
    //     user: 'root',
    //     // 密码
    //     password: 'qweasd',
    //     // 数据库名
    //     database: 'cartoon',
    //   },
    //   oa_system: {
    //     // host
    //     host: 'localhost',
    //     // 端口号
    //     port: '3306',
    //     // 用户名
    //     user: 'root',
    //     // 密码
    //     password: 'qweasd',
    //     // 数据库名
    //     database: 'oa_system',
    //   },
    // },
    // 指定默认数据库为 db1
    // default: {
    //   database: 'cartoon',
    // },
    // 是否加载到 app 上,默认开启
    app: true,
    // 是否加载到 agent 上,默认关闭
    agent: false,
    // delegate: 'model',
  };
  // add your user config here
  const userConfig = {
    // myAppName: 'egg',
  };

  return {
    ...config,
    ...userConfig,
  };
};
