'use strict';

/** @type Egg.EggPlugin */
module.exports = {
  // had enabled by egg
  // static: {
  //   enable: true,
  // }
  cors: {
    enable: true,
    package: 'egg-cors',
  },
  mysql: {
    enable: true,
    package: 'egg-mysql',
  },
  // JWT
  jwt: {
    enable: true,
    package: 'egg-jwt',
  },
  axiosPlus: {
    enable: true,
    package: 'egg-axios-plus',
  },
  redis: {
    enable: true,
    package: 'egg-redis',
  },
  oss: {
    enable: true,
    package: 'egg-oss',
  },
};
