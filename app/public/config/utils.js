'use strict';
const linkSearch = (platform, comicId, chapterId = '') => {
  console.log(platform, chapterId, '===>');
  let url = '';
  if (platform == '1' && chapterId.length > 1) {
    url = `https://comic.mkzcdn.com/chapter/content/v1/?chapter_id=${chapterId}&comic_id=${comicId}&format=1&quality=1&type=1`;
    // console.log(url, '====url');
    return url;
  }
  // eslint-disable-next-line default-case
  switch (platform) {
    case '1':
      url = `https://comic.mkzcdn.com/chapter/v1/?comic_id=${comicId}`;
      break;
  }
  return url;
};
const linkDetail = (platform, comicId) => {
  let url = '';
  // eslint-disable-next-line default-case
  switch (platform) {
    case '1':
      url = `https://comic.mkzcdn.com/comic/info/?comic_id=${comicId}`;
      break;
  }
  console.log(url, '==========');
  return url;
};
const cartoonLink = (platform, page) => {
  let url = '';
  // eslint-disable-next-line default-case
  switch (platform) {
    case '1':
      url = `https://www.mkzhan.com/category/?page=${page}`;
      break;
      case '2':
      url = `https://www.mkzhan.com/category/?page=${page}`;
      break;
  }
  console.log(url, '==========');
  return url;
};
// 去重
const reduceData = (data, value) => {
  const obj = {};
  return data.reduce((cur, next) => {
    obj[next[value]] ? '' : obj[next[value]] = true && cur.push(next);
    return cur;
  }, []);
};
module.exports = {
  linkSearch,
  linkDetail,
  cartoonLink,
  reduceData,
};
